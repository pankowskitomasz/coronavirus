import React,{Component} from "react";
import Container from "../../node_modules/react-bootstrap/Container";
import HealthcareS1 from "../components/healthcare-s1";
import HealthcareS2 from "../components/healthcare-s2";
import HealthcareS3 from "../components/healthcare-s3";

class Healthcare extends Component{
    render(){
        return(        
            <Container fluid className="minh-footer-adj p-0">
                <HealthcareS1/>
                <HealthcareS2/>
                <HealthcareS3/>
            </Container>    
        );
    }
}

export default Healthcare;