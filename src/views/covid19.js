import React,{Component} from "react";
import Container from "../../node_modules/react-bootstrap/Container";
import Covid19S1 from "../components/covid19-s1";
import Covid19S2 from "../components/covid19-s2";
import Covid19S3 from "../components/covid19-s3";

class Covid19 extends Component{
    render(){
        return(        
            <Container fluid className="minh-footer-adj p-0">
                <Covid19S1/>
                <Covid19S2/>
                <Covid19S3/>
            </Container>    
        );
    }
}

export default Covid19;